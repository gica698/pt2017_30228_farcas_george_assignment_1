
public class Monom {
    private double coeficient;
    private Integer power;

    public Monom(double coeficient, int power){
        this.coeficient = coeficient;
        this.power = power;
    }
    public double getCoeficient(){
        return coeficient;

    }
    public Integer getPower(){
        return power;
    }
    public void setCoeficient(double coeficient){
        this.coeficient = coeficient;

    }
    public void setPower(Integer power){

        this.power = power;
    }
    public String toString(){
        return coeficient + "x^" + power;
    }
}
