import java.util.List;

public class Procesare {

    public double integration(double a , double b) {
        polynomial integral= new polynomial (degree+1);
        integral.degree= degree+1;
        for (int i=0 ; i<= degree+1 ; i++){
            if (i==0) {
                integral.coefficients[i]= 0;
            }
            else {
                integral.coefficients[i]= (coefficients[i-1]/i);

            }
        }
        return (evaluate(b)- evaluate(a));
    }

    public String print(){
        if (degree ==  0) return "" + coefficients[0];
        if (degree ==  1) return coefficients[1] + "x + " + coefficients[0];
        String s = coefficients[degree] + "x^" + degree;
        for (int i = degree-1; i >= 0; i--) {
            if      (coefficients[i] == 0) continue;
            else if (coefficients[i]  > 0) s = s + " + " + ( coefficients[i]);
            else if (coefficients[i]  < 0) s = s + " - " + (-coefficients[i]);
            if      (i == 1) s = s + "x";
            else if (i >  1) s = s + "x^" + i;
        }
        return s;
    }

    public polynomial differentiate() {
        if (degree == 0) return new polynomial(0);
        polynomial derivative = new polynomial(degree - 1);
        derivative.degree = degree - 1;
        for (int i = 0; i < degree; i++){
            derivative.coefficients[i] = (i + 1) * coefficients[i + 1];
        }
        return derivative;
    }
    }
}
