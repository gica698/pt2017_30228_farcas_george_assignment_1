import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class UiController {
    Polynomial zero = new Polynomial(0, 0);

    Polynomial p1   = new Polynomial(4, 3);
    Polynomial p2   = new Polynomial(3, 2);
    Polynomial p3   = new Polynomial(1, 0);
    Polynomial p4   = new Polynomial(2, 1);
    Polynomial p    = p1.plus(p2).plus(p3).plus(p4);   // 4x^3 + 3x^2 + 1

    Polynomial q1   = new Polynomial(3, 2);
    Polynomial q2   = new Polynomial(5, 0);
    Polynomial q    = q1.plus(q2);                     // 3x^2 + 5


    Polynomial r    = p.plus(q);
    Polynomial s    = p.times(q);
    Polynomial t    = p.compose(q);

        StdOut.println("zero(x) =     " + zero);
        StdOut.println("p(x) =        " + p);
        StdOut.println("q(x) =        " + q);
        StdOut.println("p(x) + q(x) = " + r);
        StdOut.println("p(x) * q(x) = " + s);
        StdOut.println("p(q(x))     = " + t);
        StdOut.println("0 - p(x)    = " + zero.minus(p));
        StdOut.println("p(3)        = " + p.evaluate(3));
        StdOut.println("p'(x)       = " + p.differentiate());
        StdOut.println("p''(x)      = " + p.differentiate().differentiate())


}